<?php
/*----------------------------------------------
FILENAME: login-email.php
BESCHREIBUNG:   Dieses File wird aufgerufen 
sobald über jQuery Validation das Email Inputfeld 
auf dem Loginform ausgefüllt wurde. Es überprüft 
ob die eingegebene E-Mail Vorhanden ist.
------------------------------------------------*/
include '../database/connect.php';

if (!empty($_POST['email_login']))
{
    $email = mysqli_real_escape_string($db, $_POST['email_login']);
    $query = "SELECT * FROM user WHERE email = '$email'";
    $results = mysqli_query($db, $query);
    if($results->num_rows == 0)
    {
        echo "false";
    }
    else
    {
        echo "true";
    }
}
else
{
    echo "false";
}

?>
