<?php 
/*----------------------------------------------
FILENAME: page-info-request.php
BESCHREIBUNG:	Falls versucht wird einen 
Benutzer zweimal zur Mitarbeit an einer DFM 
einzuladen wird dies abgebrochen und der User 
wird über diese Seite Informiert das bereits eine
Einladung verschickt wurde. 
------------------------------------------------*/
    require('../HTML/head.html');
    require('../HTML/header_user.html');
    require('../HTML/info-request.html');
    require('../HTML/footer.html');
?>