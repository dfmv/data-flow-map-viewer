<?php 
/*----------------------------------------------
FILENAME: page-home.php
BESCHREIBUNG:	Dieses File ist dazu zuständig
die Startseite inkl. Registration für einen 
nicht Eingeloggten Benutzer der Webseite 
auszugeben. 
------------------------------------------------*/
	require('checks/visitor.php');
	require('../HTML/head.html');
	require('../HTML/header_visitor.html');
	require('../HTML/home.html');
	require('../HTML/footer.html');
?>