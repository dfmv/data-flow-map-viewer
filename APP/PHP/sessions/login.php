<?php
/*----------------------------------------------
FILENAME: login.php
BESCHREIBUNG:   Dieses File wird aufgerufen 
sobald das Loginform erfolgreich durch die jQuery 
Validation prüfung gekommen ist. Es ist dazu 
zuständig die Logindaten nochmals zu überprüfen 
und bei erfolgreichem Login eine Session zu 
starten und zu befüllen.
------------------------------------------------*/
session_start();
include("../database/connect.php");
$error = ""; 
if(isset($_POST["submit"]))
{
	if(empty($_POST["email_login"]) || empty($_POST["password_login"]))
	{
		$error = "Both fields are required.";
	}
	else
	{
		$email=$_POST['email_login'];
		$password=$_POST['password_login'];

		// Schützen vor SQL-Injection
		$email = stripslashes($email);
		$password = stripslashes($password);
		$email = mysqli_real_escape_string($db, $email);
		$password = mysqli_real_escape_string($db, $password);
		$password = md5($password);
		 
		//überprüfung ob der User Existiert
		$sql="SELECT * FROM user WHERE password ='$password' and email = '$email'";
		$result=mysqli_query($db,$sql);
		$row=mysqli_fetch_array($result,MYSQLI_ASSOC);
		if(mysqli_num_rows($result) === 1)
		{

			//erstellung der Session
			$_SESSION['email'] = $row['email']; 
			$_SESSION['id'] = $row['id'];
			$_SESSION['logged_in'] = true;
			$_SESSION['timestamp'] = time();
			header("Location: ../page-overview.php");
		}
		else
		{
			$error = "Incorrect username or password.";
		}
	}
}
if ($error != "") 
{
	echo $error;die();
}
?>