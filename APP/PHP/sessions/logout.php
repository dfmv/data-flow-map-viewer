<?php
/*----------------------------------------------
FILENAME: logout.php
BESCHREIBUNG:   Dieses File ist dazu zuständig 
eine Session zu beenden.
------------------------------------------------*/
session_start();
if(session_destroy()){
	header("Location: ../../../index.php");
}
?>