<?php
/*----------------------------------------------
FILENAME: automatic-logout.php
BESCHREIBUNG:   Dieses File wird aufgerufen 
sobald eine Seite neu geladen wird. wenn der 
Benutzer eingeloggt ist wird überprüft wie lange 
dies bereits der Fall ist, falls zuviel Zeit 
vergangen ist seit dem letzten refresh wird der 
Benutzer Automatisch ausgeloggt.
------------------------------------------------*/
if(isset($_SESSION['email'])){
      if(time() - $_SESSION['timestamp'] > 1000) { 
        echo"<script>alert('Session abgelauffen!');</script>";
        if(session_destroy()){
          header("Location: ../../index.php");

        exit;
      } else {
        $_SESSION['timestamp'] = time(); 
      }
    }
}
?>