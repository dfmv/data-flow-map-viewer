<?php
/*----------------------------------------------
FILENAME: register.php
BESCHREIBUNG:   Dieses File wird aufgerufen 
sobald das Registrationsform erfolgreich durch 
die jQuery Validation prüfung gekommen ist. Es 
ist dazu zuständig die E-Mail auf ihre 
verfügbarkeit zu prüfen und die 
Regisstrationsdaten in die Datenbank zu schreiben.
------------------------------------------------*/
include_once 'connect.php';
if(isset($_POST['submit'])){
	$error = "";
	$prename = $_POST['prename'];
	$name = $_POST['name'];
	$email1 = $_POST['email'];
	$email2 = $_POST['email_repeat'];
	$password1 = $_POST['password'];
	$password2 = $_POST['password_repeat'];
	if($email1 == $email2){
		if($password1 == $password2){
			
			//Vor SQL-Injection schützen
			$prename = stripslashes($prename);
			$name = stripslashes($name);
			$email = stripslashes($email1);
			$password = stripslashes($password1);
			$prename = mysqli_real_escape_string($db, $prename);
			$name = mysqli_real_escape_string($db, $name);
			$email = mysqli_real_escape_string($db, $email);
			$password = mysqli_real_escape_string($db, $password);
			$password = md5($password);
			
			//Verfügbarkeit der E-Mail überprüfen
			$sql = "SELECT email FROM user WHERE email='$email';";
			$result = mysqli_query($db,$sql);
			//>=
			if(mysqli_num_rows($result) == 1){
				$error = "Email already taken, please use another email adress";
			}else{
				//Registrationsdaten in Tabelle user einfügen
				$query = mysqli_query($db, "INSERT INTO user (prename,name,password,email) VALUES ('$prename', '$name','$password','$email');");
 				if($query){
 					require '../functions/registrationMail.php';
				}else{
					$error = "something went wrong with the registration";
				}
			}
		}else{
			$error = "Passwords doesn't match!";
		}	
	}else{
		$error = "Emails do not match, please enter the same adress twice.";
	}
}
if ($error != "") {
	echo $error;die();
}
?>