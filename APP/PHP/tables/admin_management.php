<?php
/*----------------------------------------------
FILENAME: admin_management.php
BESCHREIBUNG:   Dieses File erstellt die Tabelle 
"Benutzerverwaltung" für den Admin.
------------------------------------------------*/
$sql="SELECT * FROM user";
$result=mysqli_query($db,$sql);
while ($row = mysqli_fetch_array($result)) 
{
  echo "<tr>";
  echo "<td>".$row['id']."</td>";
  echo "<td>".$row['prename']."</td>";
  echo "<td>".$row['name']."</td>";
  echo "<td>".$row['email']."</td>";
  if($row['isAdmin'] === "1"){
    echo "<td> Ja </td>";
  }else{
    echo "<td> Nein </td>";
  };
  if($row['isAdmin'] === "1"){
    echo "<td> <a href='functions/revoke-admin.php?id=" . $row['id'] ."'> <span class='glyphicon glyphicon-minus'> </span> </a> </td>";
  }else{
    echo "<td> <a href='functions/grant-admin.php?id=" . $row['id'] ."'> <span class='glyphicon glyphicon-plus'> </span> </a> </td>";
  };
  if($row['isBlocked'] === "1"){
    echo "<td> Ja </td>";
  }else{
    echo "<td> Nein </td>";
  };
  if($row['isBlocked'] === "1"){
    echo "<td> <a href='functions/release-user.php?id=" . $row['id'] ."'> <span class='glyphicon glyphicon-ok'></span></a> </td>";  
  }else{
    echo "<td> <a href='functions/block-user.php?id=" . $row['id'] ."'> <span class='glyphicon glyphicon-remove'></span></a> </td>";
  };
  echo "<td> <a href='functions/delete-user.php?id=" . $row['id'] ."'> <span class='glyphicon glyphicon-trash'></span></a> </td>";
  echo "</tr>";
} 
?>