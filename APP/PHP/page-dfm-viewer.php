<?php 
/*----------------------------------------------
FILENAME: page-dfm-viewer.php
BESCHREIBUNG:	Dieses File ist dazu zuständig
die DFMs ohne bearbeitungsmöglichkeiten 
für den User auszugeben.
------------------------------------------------*/
    require('checks/dfm-viewer.php');
    require('../HTML/head.html');
    require('../HTML/header_user.html');
    require('../HTML/viewer.html');
    require('../HTML/footer.html');
?>