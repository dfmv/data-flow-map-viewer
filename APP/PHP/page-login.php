<?php 
/*----------------------------------------------
FILENAME: page-login.php
BESCHREIBUNG:	Dieses File ist dazu zuständig
die Loginseite auszugeben.
------------------------------------------------*/
	require('checks/visitor.php');
	require('../HTML/head.html');
	require('../HTML/header_visitor.html');
	require('../HTML/login.html');
	require('../HTML/footer.html');
?>