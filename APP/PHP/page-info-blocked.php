<?php 
/*----------------------------------------------
FILENAME: page-info-blocked.php
BESCHREIBUNG:	Dieses File ist dazu zuständig
eine "Block-Seite" auszugeben. Die Seite wird 
aufgerufen sobald jemand versucht ohne sich 
einzuloggen auf den DFMV zu verbinden.
------------------------------------------------*/
    require('../HTML/head.html');
    require('../HTML/header_visitor.html');
    require('../HTML/info-blocked.html');
    require('../HTML/footer.html');
?>