<?php 
/*----------------------------------------------
FILENAME: page-admin.php
BESCHREIBUNG:	Dieses File ist dazu zuständig
die Administrator-Seite auszugeben. Diese ist 
nahezu Identisch zur Projektübersichtsseite des 
Users (page-overview), mit ausnahme der 
Benutzerverwaltung und den erwiterten Funktionen
auf allen DFMs (Bearbeiten & Löschen.)
------------------------------------------------*/
    require('checks/admin.php');
    require('../HTML/head.html');
    require('../HTML/header_admin.html');
    require('../HTML/admin.html');
    require('../HTML/footer.html');
?>