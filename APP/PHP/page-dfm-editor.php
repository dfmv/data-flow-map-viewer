<?php 
/*----------------------------------------------
FILENAME: page-dfm-editor.php
BESCHREIBUNG:	Dieses File ist dazu zuständig
den DFM-Editor für den User auszugeben.
------------------------------------------------*/
    require('checks/dfm-editor.php');
    require('../HTML/head.html');
    require('../HTML/header_user.html');
    require('../HTML/editor.html');
    require('../HTML/footer.html');
?>