<?php
/*----------------------------------------------
FILENAME: overview.php
BESCHREIBUNG:   Dieses File überprüft ob der 
Benutzer eingeloggt, freigeschaltet und kein admin 
ist. Falls nicht, wird der Benutzer auf die 
"Block-Seite" (page-info-blocked) weitergeleitet 
oder auf die Adminseite (page-admin.php) falls der 
Benutzer als Admin eingeloggt ist. Diese 
Überprüfung findet nur auf page-overview.php 
statt.
------------------------------------------------*/
require 'database/connect.php';
session_start();
if(isset($_SESSION['email']))
{
	$email = $_SESSION['email'];
	$sql = mysqli_query($db,"SELECT isAdmin, isBlocked FROM user WHERE email = '$email'");

	if (mysqli_num_rows($sql) > 0) 
	{
		$row = mysqli_fetch_assoc($sql);
		$admin = $row["isAdmin"];
		$blocked = $row["isBlocked"];
		if ($blocked === "0") 
		{
			if ($admin === "0") 
			{
			}elseif ($admin === "1") 
			{
				header("Location: page-admin.php");
			}
		}else{
			header("Location: page-info-blocked.php");
		}
	}else{
		header("Location: page-info-blocked.php");
	}
}else{
		header("Location: page-info-blocked.php");
	}
?>