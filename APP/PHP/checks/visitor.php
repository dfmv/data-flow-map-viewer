<?php
/*----------------------------------------------
FILENAME: visitor.php
BESCHREIBUNG:   Dieses File überprüft ob der 
Benutzer eingeloggt und freigeschaltet ist, falls 
der User eingeloggt ist wird ausserdem überprüft 
ob der Benutzer ein user oder Admin ist, user 
werden auf page-overview.php weitergeleitet, 
admins werden auf page-admin.php weitergeleitet. 
Falls nicht, wird nichts ausgeführt. Diese 
Überprüfung findet nur auf page-home.php und 
page-login.php statt.
------------------------------------------------*/
require 'database/connect.php';
session_start();
if(isset($_SESSION['email'])){
	$email = $_SESSION['email'];
	$sql = mysqli_query($db,"SELECT isAdmin, isBlocked FROM user WHERE email = '$email'");

	if (mysqli_num_rows($sql) > 0) {
		$row = mysqli_fetch_assoc($sql);
		$admin = $row["isAdmin"];
		$blocked = $row["isBlocked"];
		if ($blocked === "0") {
			if ($admin === "1") {
				header("Location: page-admin.php");
			}else{
				header("Location: page-overview.php");
			}
		}
	}
}
?>