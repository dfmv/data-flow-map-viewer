<?php
/*----------------------------------------------
FILENAME: admin.php
BESCHREIBUNG:   Dieses File überprüft ob der 
Benutzer eingeloggt, freigeschaltet und Admin ist.
Falls nicht, wird der Benutzer auf die 
"Block-Seite" (page-info-blocked) weitergeleitet 
oder auf das DFM-verzeichnis (page-overview.php) 
Projektverzeichnis falls der Benutzer als User 
eingeloggt ist. Diese Überprüfung findet nur auf 
page-admin.php statt.
------------------------------------------------*/
require 'database/connect.php';
session_start();
if(isset($_SESSION['email']))
{
	$email = $_SESSION['email'];
	$sql = mysqli_query($db,"SELECT isAdmin, isBlocked FROM user WHERE email = '$email'");
	if (mysqli_num_rows($sql) > 0) 
	{
		$row = mysqli_fetch_assoc($sql);
		$admin = $row["isAdmin"];
		$blocked = $row["isBlocked"];
		if ($blocked === "0") 
		{
			if ($admin === "1") 
			{
			}else{
				header("Location: page-overview.php");
			}
		}else{
			header("Location: page-info-blocked.php");
		}
	}else{
		header("Location: page-info-blocked.php");
	}
}else{
		header("Location: page-info-blocked.php");
	}
?>