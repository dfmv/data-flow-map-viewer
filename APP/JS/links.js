//Seiten ohne Funktionen werden über die ID des Main Tags der HTML Seite mittels jQuery geladen.
$(document).ready(function(){
  $("#notation").click(function(){
    $("#main").load("../HTML/load/notation.html");
  });  
  $("#help").click(function(){
    $("#main").load("../HTML/load/help.html");
  });
  $("#impressum").click(function(){
    $("#main").load("../HTML/load/impressum.html");
  });
  $("#contact").click(function(){
    $("#main").load("../HTML/load/contact.html");
  });
});

