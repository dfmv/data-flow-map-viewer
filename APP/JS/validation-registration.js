//Überprüfen der Eingaben des Login-Forms
$(function(){
	$("#registration-form").validate({
		onkeyup: false,
		rules: {
			prename: {
				required: true,
				minlength: 2,
				lettersonly: true
			},
			name: {
				required: true,
				minlength: 2,
				lettersonly: true
			},
			email: {
                required: true,
                swisscomEmail: true,
                //überprüft ob die Email bereits verwendet wurde
                remote: {
                  	url: '../PHP/validation/registration-email.php',
                  	type: "post",
                  	data: {
                      	email: function() {
                          	return $('#register-email').val();
                      	}
                  	}
                }
            },
			email_repeat: {
				required: true,
				swisscomEmail: true,
				equalTo: "#register-email"
			},
			password: {
				required: true,
				passwordRegex: true
			},
			password_repeat: {
				required: true,
				equalTo: "#register-pw",
				passwordRegex: true
			}
		},
		messages: {
			prename: {
				required: "Please enter your prename",
				minlength: "Your Prename must at least contain two characters",
				lettersonly: "Letters only"
			},
			name: {
				required: "Please enter your name",
				minlength: "Your Name must at least contain two characters",
				lettersonly: "Letters only"
			},
			email: {
				required: "Please enter your email adress",
				email: "please enter a valid email adress",
				remote: "email is already taken."
			},
			email_repeat: {
				required: "Please repeat your email adress",
				email: "please enter a valid email adress",
				equalTo: "this email adress has to be equal to the email above"
			},
			password: {
				required: "Please enter your password"
			},
			password_repeat: {
				required: "Please repeat your password",
				equalTo: "this password has to be equal to the password above"
			}
		}
	});
	//Überprüft ob nur Buchstaben verwendet werden
	jQuery.validator.addMethod("lettersonly", function(value, element) {
	  return this.optional(element) || /^[a-z]+$/i.test(value);
	}, "Letters only please"); 
	//Überprüft ob nur Buchstaben verwendet werden
	$.validator.addMethod("swisscomEmail", function(value, element) {
		return this.optional(element) || /^.+@swisscom.com$/.test(value);
	}, "Only swisscom.com email adresses are allowed");
	//Überprüft ob das Passwort den Passwortanforderungen entspricht
	$.validator.addMethod("passwordRegex", function(value, element) {
		return this.optional(element) || /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*\_\-])(?=.{8,})/.test(value);
	}, "ihr Passwort muss mindestens 8 Zeichen lang sein und Folgende anforderungen erfüllen, das Passwort enthält mindestens einen: Grossbuchstaben, Kleinbuchstaben, Nummer, Sonderzeichen");


});