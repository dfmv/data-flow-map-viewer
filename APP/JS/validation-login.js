//Überprüfen der Eingaben des Login-Forms
$(function(){
    $(".login-form").validate({
        onkeyup: false,
        rules: {
            email_login: {
                required: true,
                swisscomEmail: true,
                //überprüft ob ein User mit dieser E-Mail existiert
                remote: {
                    url: '../PHP/validation/login-email.php',
                    type: "post",
                    data: {
                        email_login: function() {
                            return $('#email_login').val();
                        }
                    }
                }
            },
            password_login: {
                required: true,
                //überprüft ob ein User mit diesem Passwort existiert
                remote: {
                    url: '../PHP/validation/login-password.php',
                    type: "post",   
                    data: {
                        password_login: function() {
                            return $('#password_login').val();;
                        }
                    }
                }
            }
        },
        messages: {
            email_login: {
                required: "Please enter your email adress",
                swisscomEmail: "please enter a valid email adress",
                remote: "email does not exist."
            },
            password_login: {
                required: "Please enter your password",
                remote: "Password is wrong"
            }
        },
    });
    //Überprüft ob eine Swisscom-Email verwendet wurde
    $.validator.addMethod("swisscomEmail", function(value, element) {
        return this.optional(element) || /^.+@swisscom.com$/.test(value);
    }, "Only swisscom.com email adresses are allowed");
});