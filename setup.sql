DROP DATABASE IF EXISTS `Kyuubi`;
CREATE DATABASE `Kyuubi`;
USE `Kyuubi`;
# ------------------------------------------------------------
DROP TABLE IF EXISTS `coworker`;
CREATE TABLE `coworker` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `dfm_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `dfm_id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
# ------------------------------------------------------------
DROP TABLE IF EXISTS `DFM`;
CREATE TABLE `DFM` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL DEFAULT '',
  `creator_id` int(11) NOT NULL,
  `source` varchar(1000) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
# ------------------------------------------------------------
DROP TABLE IF EXISTS `invitations`;
CREATE TABLE `invitations` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `invitation_from` int(11) NOT NULL,
  `invitation_to` int(11) NOT NULL,
  `invitation_dfm` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
# ------------------------------------------------------------
DROP TABLE IF EXISTS `requests`;
CREATE TABLE `requests` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `request_to` int(11) NOT NULL,
  `request_dfm` int(11) NOT NULL,
  `request_from` int(11) NOT NULL,
  `sent` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
# ------------------------------------------------------------
DROP TABLE IF EXISTS `user`; 
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prename` varchar(30) NOT NULL DEFAULT '',
  `name` varchar(30) NOT NULL DEFAULT '',
  `password` varchar(60) NOT NULL DEFAULT '',
  `email` varchar(60) NOT NULL DEFAULT '',
  `isAdmin` tinyint(1) NOT NULL DEFAULT '0',
  `isBlocked` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
# ------------------------------------------------------------
INSERT INTO user(prename,name,password,email,isAdmin,isBlocked)
VALUES ("Sebastian","Salvisberg","ed7edcc801715a634901f3d22db37451","sebastian.salvisberg@swisscom.com",1,0);

INSERT INTO user(prename,name,password,email,isAdmin,isBlocked)
VALUES ("Raphael","Bucher","791f19725bc33c489ee92082cd6c55a7","raphael.bucher@swisscom.com",1,0);

